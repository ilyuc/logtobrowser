package com.ilyuc.logtobrowser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogtobrowserApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogtobrowserApplication.class, args);
    }

}
